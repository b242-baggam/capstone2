
const express= require("express");
const User=require("../models/Users");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Products=require("../models/Products");

//resister user

module.exports.resisterUser=(reqBody)=>{
	let newUser=new User({
		email:reqBody.email,
		password:bcrypt.hashSync(reqBody.password, 10)

	})

	return newUser.save().then((user, error) =>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

//user Authentication

module.exports.loginUser=(reqBody)=>{
	return User.findOne({email:reqBody.email}).then(result=>{
		if(result ==null){
			return false;
		}

		{
			const isPasswordCorrect= bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return {acess: auth.createAccessToken(result)}
			}
			else{
				return false;
			}
		}
		
	})
}