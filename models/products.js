const mongoose=require("mongoose");

const productSchema= new mongoose.Schema({
	name: {
		type: String
	},

	description: String,

	price: Number,

	isActive: {
		type: Boolean,
		default: true
	},
	createdOn:{
		type: Date,
		default: new Date()
	},

	orders:[
	{orderId:String}]
})

module.exports= mongoose.model("products", productSchema)