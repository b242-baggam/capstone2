const mongoose= require("mongoose");

const usersSchema= new mongoose.Schema({
	email: {
		type: String,
		require: [true, "email is required"]
	       },

	password:{
		type:String,
		require:[true, "password is required"]
	},

	isAdmin: {
		type: Boolean,
		default: false
	},

	orderes:[
		{
			products:[
				{
					productName:{
						type: String
					}

				},

				{
					quantity:{
						type: Number
					}
				}
			]
		},

		{
			totalAmount: Number
		},

		{
			PurchasedOn:{
				type: Date,
				default: new Date ()
			}
		}
	]


})

module.exports= mongoose.model("user", usersSchema)