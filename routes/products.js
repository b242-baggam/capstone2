const express= require ("express");
const router=express.Router();

const productController=require("../controllers/products");
const auth = require("../auth");

module.exports=router;