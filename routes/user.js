const express= require("express");
const router=express.Router();
const userController=require("../controllers/users");
const auth = require("../auth");

//user registration
router.post("/register", (req, res) =>{
	userController.resisterUser(req.body).then(resultFromController => res.send(resultFromController));
})

//user Authentication
router.post("/login",(req, res)=>{
	userController.loginUser(req.body).then(resultFromController=>res.send(resultFromController));
})




module.exports= router;